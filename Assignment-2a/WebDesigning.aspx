﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="WebDesigning.aspx.cs" Inherits="Assignment_2a.WebDesigning" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Idea" runat="server">
    <h3><u>WEB DESIGNING</u></h3>
    <div class="jumbotron">
        In 1996, Microsoft released its first competitive browser, which was complete with its own features and tags. It was also the first browser to support style sheets, which at the time was seen as an obscure authoring technique.[5] The HTML markup for tables was originally intended for displaying tabular data. However designers quickly realized the potential of using HTML tables for creating the complex, multi-column layouts that were otherwise not possible.
    </div>

</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="code1" runat="server">
    <div class="jumbotron">
         However, because Flash required a plug-in, many web developers avoided using it for fear of limiting their market share due to lack of compatibility. Instead, designers reverted to gif animations (if they didn't forego using motion graphics altogether) and JavaScript for widgets. But the benefits of Flash made it popular enough among specific target markets to eventually work its way to the vast majority of browsers, and powerful enough to be used 
    </div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="code2" runat="server">
    <div class="jumbotron">
        Website designers may consider it to be good practice to conform to standards. This is usually done via a description specifying what the element is doing. Failure to conform to standards may not make a website unusable or error prone, but standards can relate to the correct layout of pages for readability as well making sure coded elements are closed appropriately. This includes errors in code, more organized layout for code, and making sure IDs and classes are identified properly
    </div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="links" runat="server">
       <div class="jumbotron">
   Useful Links:
   <ul>
       <li><a href="https://www.w3schools.com/">W3SCHOOLS</a></li>
       <li><a href="https://www.codecademy.com/learn/learn-html">Codecademy</a></li>
       <li><a href="https://html.com/">HTML.COM</a></li>
   </ul>
    </div>
</asp:Content>
