﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Javascript.aspx.cs" Inherits="Assignment_2a.Javascript" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Idea" runat="server">
    <div class="jumbotron">
        <h3><u>JAVASCRIPT</u></h3>
<p>JavaScript was first released with Netscape 2 early in 1996. It was originally going to be called LiveScript, but it was renamed in an ill-fated marketing decision that attempted to capitalize on the popularity of Sun Microsystem's Java language — despite the two having very little in common. 
</p></div>
</asp:Content>
<asp:Content ID="Content2" ContentPlaceHolderID="code1" runat="server">
<div class="jumbotron">
<p>JavaScript is a multi-paradigm, dynamic language with types and operators, standard built-in objects, and methods. Its syntax is based on the Java and C languages — many structures from those languages apply to JavaScript as well. JavaScript supports object-oriented programming with object prototypes, instead of classes
</p></div>
</asp:Content>
<asp:Content ID="Content3" ContentPlaceHolderID="code2" runat="server">
<div class="jumbotron">
<p>In practice, integer values are treated as 32-bit ints, and some implementations even store it that way until they are asked to perform an instruction that's valid on a Number but not on a 32-bit integer. This can be important for bit-wise operations.
</p></div>
</asp:Content>
<asp:Content ID="Content4" ContentPlaceHolderID="links" runat="server">
    <div class="jumbotron">
    Useful Links:
   <ul>
       <li><a href="https://www.w3schools.com/">W3SCHOOLS</a></li>
       <li><a href="https://www.codecademy.com/learn/learn-javascript">Codecademy</a></li>
       <li><a href="https://www.learn-js.org/">LEARN-JS</a></li>
   </ul>
        </div>
</asp:Content>
