﻿<%@ Page Title="" Language="C#" MasterPageFile="~/Site.Master" AutoEventWireup="true" CodeBehind="Database.aspx.cs" Inherits="Assignment_2a.Database" %>
<asp:Content ID="Content1" ContentPlaceHolderID="Idea" runat="server">
    <div class="jumbotron">
        <h3><u>DATABASE</u></h3>
    When you have some data, and you want to store this data some where. This data could be anything. It could be about customers, products, employees, orders, …etc. This data could be in text format, numeric, dates, document files, images, audio, or video.Maybe if you have data about the customers in your company, the first thing that comes in mind is to open a spreadsheet. Then you start write whatever the data you want to store.
    </div>
</asp:Content>
    
    <asp:Content ID="Content2" ContentPlaceHolderID="code1" runat="server">
      <div class="jumbotron">
          INSERT INTO table2
SELECT * FROM table1
WHERE condition;
    </div>  
</asp:Content>


<asp:Content ID="Content3" ContentPlaceHolderID="code2" runat="server">
   
    <div class="jumbotron">
        INSERT INTO table2 (column1, column2, column3, ...)
SELECT column1, column2, column3, ...
FROM table1
WHERE condition;
    </div>
</asp:Content>


<asp:Content ID="Content4" ContentPlaceHolderID="links" runat="server">
   <div class="jumbotron">

    Useful Links:
   <ul>
       <li><a href="https://www.w3schools.com"/>W3SCHOOLS</li>
       <li><a href="https://www.udemy.com/learn-database-design-with-mysql"/>Udemy</li>
       <li><a href="https://www.quackit.com/database/tutorial"/>Quackit</li>
   </ul>
   </div>
</asp:Content>
